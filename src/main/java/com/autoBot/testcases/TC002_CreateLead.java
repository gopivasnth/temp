package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;
// Annotations == ProjectBase
public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Login into leaftaps";
		author = "koushik";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String uName, String pwd,String cn, String fn, String ln) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		//.clickLogout();
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cn)
		.enterFirstName(fn)
		.enterLastName(ln);
		
		
		
		
		
		
	}
	
}






